﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace HankWMS_Api.CustomException
{
    public class ClientException : Exception
    {
        /// <summary>
        /// 响应状态码
        /// </summary>
        public HttpStatusCode StatusCode { get; set; }

        public ClientException(HttpStatusCode statusCode)
        {
            StatusCode = statusCode;
        }
    }
}
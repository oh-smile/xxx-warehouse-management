﻿using HankWMS_Api.BLL;
using HankWMS_Api.CustomException;
using HankWMS_Api.Models;
using HankWMS_Api.Utils;
using System;
using System.Net;
using System.Web.Http;

namespace HankWMS_Api.Controllers
{
    /// <summary>
    /// 账户相关接口
    /// </summary>
    public class AccountController : ApiController
    {

        public UserManager accountManager = new UserManager();


        /// <summary>
        /// 登录接口
        /// </summary>
        [Route("api/Account/Login")]
        public R<UserVO> Login([FromBody] LoginFormModel model)
        {
            // 数据校验
            if (!ModelState.IsValid)
            {
                // 标准的返回,返回HTTPP的状态码400
                // [200 ~ 300) 成功
                // [300 ~ 400) 重定向
                // 400 错误的请求
                // 401 未授权
                // 403 禁止访问
                // 404 访问的资源不存在
                // 500 服务器错误
                throw new ClientException(HttpStatusCode.BadRequest);
            }
            // 逻辑代码
            var user = accountManager.Login(model);
            if (user != null)
            {
                // 生成token
                // 准备存入token的数据
                AuthInfo authInfo = new AuthInfo();
                authInfo.Id = user.Id;
                authInfo.Name = user.NickName;
                authInfo.CreateAt = DateTime.Now;
                authInfo.ExpiredAt = DateTime.Now.AddMinutes(5);
                var token = JwtHelper<AuthInfo>.Generate(authInfo);
                return new R<UserVO>()
                {
                    Code = "000000",
                    Message = "登录成功",
                    Data = new UserVO
                    {
                        NickName = user.NickName,
                        Token = "Bearer " + token
                    }
                };
            }
            else
            {
                return new R<UserVO>() { Code = "999999", Message = "登录失败，用户名或密码错误" };
            }
        }


    }
}

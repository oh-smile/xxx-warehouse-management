﻿using HankWMS_Api.BLL;
using HankWMS_Api.CustomException;
using HankWMS_Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace HankWMS_Api.Controllers
{
    /// <summary>
    /// 仓库管理
    /// </summary>
    public class WarehouseController : ApiController
    {
        /// <summary>
        /// 仓库管理的业务逻辑层
        /// </summary>
        private WarehouseManager warehouseManager = new WarehouseManager();

        private UserManager userManager = new UserManager();

        /// <summary>
        /// 获取管理员列表
        /// </summary>
        /// <returns></returns>
        [Route("api/Warehouse/Users")]
        public R<List<OptionVO>> Get()
        {
            List<UserDto> users = userManager.GetAll();
            List<OptionVO> options = users.Select(userDto => new OptionVO() { Value = userDto.Id, Label = userDto.NickName }).ToList();
            return new R<List<OptionVO>>() { Code = "000000", Data = options };
        }

        /// <summary>
        /// 获取列表
        /// </summary>
        /// <returns></returns>
        public R<WarehousePageVO> Get([FromUri] WarehouseQueryFormModel model)
        {
            // 判断是否由传递参数，如果没有传递，则初始一个默认值
            if (model == null)
            {
                model = new WarehouseQueryFormModel();
            }

            // 查询列表
            var list = warehouseManager.FindPage(model);

            // 查询总记录
            var total = warehouseManager.Count(model.SearchText);

            // 组装要返回的数据
            var pageVo = new R<WarehousePageVO>();
            pageVo.Message = "查询成功";
            pageVo.Code = "000000";
            pageVo.Data = new WarehousePageVO();
            pageVo.Data.Total = total;
            pageVo.Data.List = list;
            pageVo.Data.PageIndex = model.PageIndex;
            pageVo.Data.PageSize = model.PageSize;
            // 返回结果
            return pageVo;
        }

        /// <summary>
        /// 添加仓库
        /// </summary>
        /// <returns></returns>
        public R<string> Post([FromBody] WarehouseInsertFormModel model)
        {
            if (!ModelState.IsValid)
            {
                // 400 Bad Request
                throw new ClientException(HttpStatusCode.BadRequest);
                //throw new Exception("自定义异常消息");
            }
            if (warehouseManager.Insert(model))
            {
                return new R<string> { Code = "000000", Message = "添加成功" };
            }
            else
            {
                return new R<string> { Code = "999999", Message = "添加失败" };
            }

        }


        /// <summary>
        /// 更新仓库
        /// </summary>
        /// <returns></returns>
        public R<string> Put([FromBody] WarehouseEditFormModel model)
        {
            if (!ModelState.IsValid)
            {
                throw new ClientException(HttpStatusCode.BadRequest);
            }
            if (warehouseManager.Update(model))
            {
                return new R<string> { Code = "000000", Message = "更新成功" };
            }
            else
            {
                return new R<string> { Code = "999999", Message = "更新失败" };
            }
        }


        /// <summary>
        /// 删除仓库
        /// </summary>
        /// <returns></returns>
        public R<string> Delete(int id)
        {
            if (warehouseManager.Delete(id))
            {
                return new R<string> { Code = "000000", Message = "删除成功" };
            }
            else
            {
                return new R<string> { Code = "999999", Message = "删除失败" };
            }
        }


    }
}

﻿using again.BLL;
using again.DAL;
using again.Entities;
using again.Models;
using HankWMS_Api.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace again.Controllers
{                                                  //表结构      查询条件
    public class ProductController : BaseController<Product,ProductQueryFormModel>
    {
        public ProductController()
        {
            //获取数据
            var service = new ProductService();
            _baseManager = new ProductManager<ProductQueryFormModel>(service);
        }
    }
}

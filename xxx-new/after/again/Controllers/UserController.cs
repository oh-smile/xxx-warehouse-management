﻿using HankWMS_Api.BLL;
using HankWMS_Api.CustomAttribute;
using HankWMS_Api.CustomException;
using HankWMS_Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Routing;
using System.Xml.Schema;

namespace HankWMS_Api.Controllers
{
    /// <summary>
    /// 用户管理接口
    /// </summary>
    [ApiAuthorize]
    public class UserController : ApiController
    {
        private UserManager userManager = new UserManager();


        /// <summary>
        /// 获取单个用户信息
        /// </summary>
        /// <param name="id">用户id</param>
        /// <returns></returns>
        public R<UserDto> Get(int id)
        {
            var user = userManager.FindById(id);
            if (user == null)
            {
                return new R<UserDto> { Code = "999999", Message = "获取失败", Data = user };
            }

            return new R<UserDto> { Code = "000000", Message = "获取成功", Data = user };
        }

        /// <summary>
        /// 获取多个用户信息
        /// </summary>
        /// <returns></returns>
        [Route("api/Users")]
        public R<UserPageVO> GetList([FromUri] UserQueryFormModel model)
        {
            if (model == null)
            {
                model = new UserQueryFormModel();
            }
            List<UserDto> list = userManager.FindPage(model);
            var total = userManager.Count(model.SearchText);
            UserPageVO userPage = new UserPageVO();
            userPage.List = list;
            userPage.Total = total;
            userPage.PageSize = model.PageSize;
            userPage.PageIndex = model.PageIndex;

            return new R<UserPageVO> { Code = "000000", Message = "操作成功", Data = userPage };
        }

        /// <summary>
        /// 添加用户
        /// </summary>
        /// <returns></returns>
        public R<string> Post([FromBody] UserInsertFormModel model)
        {
            if (!ModelState.IsValid)
            {
                // 400 Bad Request
                throw new ClientException(HttpStatusCode.BadRequest);
                //throw new Exception("自定义异常消息");
            }
            if (userManager.Insert(model))
            {
                return new R<string> { Code = "000000", Message = "添加成功" };
            }
            else
            {
                return new R<string> { Code = "999999", Message = "添加失败" };
            }

        }


        /// <summary>
        /// 更新用户
        /// </summary>
        /// <returns></returns>
        public R<string> Put([FromBody] UserEditFormModel model)
        {
            if (!ModelState.IsValid)
            {
                throw new ClientException(HttpStatusCode.BadRequest);
            }
            if (userManager.Update(model))
            {
                return new R<string> { Code = "000000", Message = "更新成功" };
            }
            else
            {
                return new R<string> { Code = "999999", Message = "更新失败" };
            }
        }

        /// <summary>
        /// 修改密码
        /// </summary>
        /// <returns></returns>
        [Route("api/User/ChangePwd")]
        public R<string> Put([FromBody] ChangePwdFormModel model)
        {
            if (!ModelState.IsValid)
            {
                throw new ClientException(HttpStatusCode.BadRequest);
            }
            if (userManager.ChangePwd(model))
            {
                return new R<string> { Code = "000000", Message = "更新成功" };
            }
            else
            {
                return new R<string> { Code = "999999", Message = "更新失败" };
            }
        }


        /// <summary>
        /// 删除用户
        /// </summary>
        /// <returns></returns>
        public R<string> Delete(int id)
        {
            if (userManager.Delete(id))
            {
                return new R<string> { Code = "000000", Message = "删除成功" };
            }
            else
            {
                return new R<string> { Code = "999999", Message = "删除失败" };
            }
        }

    }
}

﻿using again.BLL;
using again.DAL;
using again.Entities;
using again.Models;
using HankWMS_Api.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace again.Controllers
{
    public class SupplierController : BaseController<Supplier,SupplierQueryFormModel>
    {
        public SupplierController()
        {
            var service = new SupplierService();
            _baseManager=new SupplierManager<SupplierQueryFormModel>(service);
            
        }
    }
}

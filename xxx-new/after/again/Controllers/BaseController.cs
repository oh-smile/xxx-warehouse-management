﻿using again.BLL;
using again.Entities;
using again.Models;
using HankWMS_Api.BLL;
using HankWMS_Api.CustomException;
using HankWMS_Api.DAL;
using HankWMS_Api.Entities;
using HankWMS_Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace again.Controllers
{
    /// <summary>
    /// 共用控制器父类
    /// </summary>
    /// <typeparam name = "T" > 数据库的表对应的模型类 </ typeparam >
    /// < typeparam name="K">查询条件</typeparam>
    /// ApiController是一个ASP.NET Web API框架中的基类，用于创建Web API控制器。Web API控制器是用于处理HTTP请求并返回HTTP响应的类。
    /// 当一个类继承自ApiController时，它就成为一个Web API控制器，并可以处理客户端发送的HTTP请求。在这段代码中，BaseController<T,K>继承自ApiController，表示BaseController<T,K>是一个Web API控制器
    /// 通过继承ApiController，BaseController<T,K>可以使用Web API框架提供的一些特性和功能，例如路由配置、参数绑定、返回类型处理等。它可以定义一些Web API的行为和操作，以处理客户端的请求并返回相应的HTTP响应。
    public abstract class BaseController<T, K> : ApiController where T : BaseEntity
    {
        /// <summary>
        /// 业务逻辑层
        /// </summary>
        protected BaseManager<T, K> _baseManager;


        public BaseController()
        {

        }

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="baseManager"></param>
        public BaseController(BaseManager<T, K> baseManager)
        {
            _baseManager = baseManager;
        }


        /// <summary>
        /// 获取列表
        /// </summary>
        /// <returns></returns>
        public R<BasePageVO<T>> Get([FromUri] BasePageQueryFormModel<K> model)
        {
            // 判断是否由传递参数，如果没有传递，则初始一个默认值
            if (model == null)
            {
                model = new BasePageQueryFormModel<K>();
            }

            // 查询列表
            var list = _baseManager.FindPage(model);

            // 查询总记录
            var total = _baseManager.Count(model.QueryParams);

            // 组装要返回的数据
            var pageVo = new R<BasePageVO<T>>();
            pageVo.Message = "查询成功";
            pageVo.Code = "000000";
            pageVo.Data = new BasePageVO<T>();
            pageVo.Data.Total = total;
            pageVo.Data.List = list;
            pageVo.Data.PageIndex = model.PageIndex;
            pageVo.Data.PageSize = model.PageSize;

            // 返回结果
            return pageVo;
        }

        /// <summary>
        /// 添加
        /// </summary>
        /// <returns></returns>
        public R<string> post([FromBody] T model)
        {
            if (!ModelState.IsValid)
            {
                // 400 Bad Request
                throw new ClientException(HttpStatusCode.BadRequest);
                //throw new Exception("自定义异常消息");
            }
            if (_baseManager.Insert(model))
            {
                return new R<string> { Code = "000000", Message = "添加成功" };
            }
            else
            {
                return new R<string> { Code = "999999", Message = "添加失败" };
            }

        }


        /// <summary>
        /// 更新
        /// </summary>
        /// <returns></returns>
        public R<string> Put([FromBody] T model)
        {
            if (!ModelState.IsValid)
            {
                throw new ClientException(HttpStatusCode.BadRequest);
            }
            if (_baseManager.Update(model))
            {
                return new R<string> { Code = "000000", Message = "更新成功" };
            }
            else
            {
                return new R<string> { Code = "999999", Message = "更新失败" };
            }
        }


        /// <summary>
        /// 删除
        /// </summary>
        /// <returns></returns>
        public R<string> Delete(int id)
        {
            if (_baseManager.Delete(id))
            {
                return new R<string> { Code = "000000", Message = "删除成功" };
            }
            else
            {
                return new R<string> { Code = "999999", Message = "删除失败" };
            }
        }


    }
}

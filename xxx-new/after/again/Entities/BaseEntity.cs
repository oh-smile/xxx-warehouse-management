﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace again.Entities
{
    public abstract class BaseEntity
    {
        /// <summary>
        /// id
        /// </summary>
        public int Id { get; set; }
    }
}
﻿using again.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace HankWMS_Api.Entities
{
    /// <summary>
    /// 仓库表的表结构
    /// </summary>
    public class Warehouse:BaseEntity
    {
        /// <summary>
        /// id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// 仓库名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 仓库地址
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// 仓库管理员id
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// 管理员信息（导航属性）
        /// </summary>
        public virtual User Manager { get; set; }

        /// <summary>
        /// 联系电话
        /// </summary>
        public string PhoneNumber { get; set; }

    }
}
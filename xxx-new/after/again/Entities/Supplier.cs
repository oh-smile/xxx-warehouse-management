﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace again.Entities
{
    /// <summary>
    /// 供应商表的结构
    /// </summary>
    public class Supplier:BaseEntity
    {
        /// <summary>
        /// id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 供应商名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 供应商地址
        /// </summary>
        public string Address { get; set; }
        /// <summary>
        /// 联系人
        /// </summary>
        public string ContactPerson { get; set; }
        /// <summary>
        /// 联系电话
        /// </summary>
        public string ContactModile { get; set; }

    }
}
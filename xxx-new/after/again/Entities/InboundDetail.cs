﻿using HankWMS_Api.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace again.Entities
{
    /// <summary>
    /// 入库明细表的结构
    /// </summary>
    public class InboundDetail:BaseEntity
    {
        /// <summary>
        /// 入库明细id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 入库id
        /// </summary>
        public int InboundId { get; set; }
        /// <summary>
        /// 商品id
        /// </summary>
        public int  ProductId { get; set; }
        /// <summary>
        /// 数量
        /// </summary>
        public int Quantity { get; set; }
        /// <summary>
        /// 仓库id
        /// </summary>
        public int WarehouseId { get; set; }
        /// <summary>
        ///具体入库主表信息
        /// </summary>
        public virtual Inbound InboundInfo { get; set; }
        /// <summary>
        /// 具体商品信息
        /// </summary>
        public virtual Product ProductInfo { get; set; }
        /// <summary>
        /// 具体仓库信息
        /// </summary>
        public virtual Warehouse WarehouseInfo { get; set; }


    }
}
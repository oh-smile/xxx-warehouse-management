﻿using HankWMS_Api.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace again.Entities
{
    /// <summary>
    /// 入库表结构
    /// </summary>
    public class Inbound:BaseEntity
    {
        /// <summary>
        /// 入库日期
        /// </summary>
        public DateTime Date { get; set; }
        /// <summary>
        /// 入库的类型
        /// </summary>
        public string Method { get; set; }
        /// <summary>
        /// 入库人id
        /// </summary>
        public int UserId { get; set; }
        /// <summary>
        /// 入库人信息（导航属性）
        /// </summary>
        public virtual User UserInfo { get; set; }

    }
}
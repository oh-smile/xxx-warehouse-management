﻿using HankWMS_Api.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace again.Entities
{
    /// <summary>
    /// 商品库存
    /// </summary>
    public class ProductStock:BaseEntity
    {
        /// <summary>
        /// 库存id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 商品id
        /// </summary>
        public int ProductId { get; set; }
        /// <summary>
        /// 仓库id
        /// </summary>
        public int warehourseId { get; set; }

        /// <summary>
        /// 商品详细信息（导航属性）
        /// </summary>
        public virtual Product ProductInfo { get; set; }
        /// <summary>
        /// 所在仓库的详细信息（导航属性）
        /// </summary>
        public virtual Warehouse WarehouseInfo { get; set; }
    }
}
﻿using again.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HankWMS_Api.Entities
{
    // 类 和 数据库中的表是一一对应的
    /// <summary>
    /// 用户表模型,对应数据库表结构
    /// </summary>
    public class User : BaseEntity
    {
        /// <summary>
        /// Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// 用户名
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// 昵称
        /// </summary>
        public string NickName { get; set; }

        /// <summary>
        /// 管理的仓库（导航属性）
        /// </summary>
        public ICollection<Warehouse> Warehouses { get; set; } = new List<Warehouse>();
        /// <summary>
        /// 出库的记录（导航属性）
        /// </summary>
        public ICollection<Outbound> Outbounds { get; set; }=new List<Outbound>();
        /// <summary>
        /// 入库的记录（导航属性）
        /// </summary>
        public ICollection<Inbound> Inbounts { get; set; } = new List<Inbound>();

    }
}
﻿using again.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace HankWMS_Api.Entities
{
    /// <summary>
    /// 数据库EF模型
    /// </summary>
    public class HankWMSContext : DbContext
    {
        /// <summary>
        /// 
        /// </summary>
        public HankWMSContext() : base("server=.;database=HankWMS;user=sa;password=123456;")
        {

        }

        // 属性 -> 表
        /// <summary>
        /// 用户
        /// </summary>
        public DbSet<User> Users { get; set; }

        /// <summary>
        /// 仓库
        /// </summary>
        public DbSet<Warehouse> Warehouses { get; set; }
        /// <summary>
        /// 供应商
        /// </summary>
        public DbSet<Supplier> Suppliers { get; set; }
        /// <summary>
        /// 入库
        /// </summary>
        public DbSet<Inbound> Inbounds { get; set; }
        /// <summary>
        /// 入库明细
        /// </summary>
        public DbSet<InboundDetail> InboundDetails { get; set; }
        /// <summary>
        /// 出库
        /// </summary>
        public DbSet<Outbound> Outbounds { get; set; }

        /// <summary>
        /// 出库明细
        /// </summary>
        public DbSet<OutboundDetail> OutboundDetails { get; set; }
        /// <summary>
        /// 商品
        /// </summary>
        public DbSet<Product>Products { get; set; }
        /// <summary>
        /// 商品库存
        /// </summary>
        public DbSet<ProductStock> ProductStocks { get; set; }

    }
}
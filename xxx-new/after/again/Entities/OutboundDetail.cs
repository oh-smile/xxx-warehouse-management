﻿using HankWMS_Api.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace again.Entities
{
    /// <summary>
    /// 出库明细表的结构
    /// </summary>
    public class OutboundDetail:BaseEntity
    {
        /// <summary>
        /// 出库明细id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// 出库id
        /// </summary>
        public int OutboundId { get; set; }
        /// <summary>
        /// 商品id
        /// </summary>
        public int ProductId { get; set; }
        /// <summary>
        /// 数量
        /// </summary>
        public int Quantity { get; set; }
        /// <summary>
        /// 仓库id
        /// </summary>
        public int WarehouseId { get; set; }
        /// <summary>
        /// 具体的出库主表信息（导航属性）
        /// </summary>
        public virtual Outbound OutbountInfo { get; set; }
        /// <summary>
        /// 具体的商品表信息（导航属性）
        /// </summary>
        public virtual Product ProductInfo { get; set; }
        /// <summary>
        /// 具体仓库信息（导航属性）
        /// </summary>
        public virtual Warehouse WarehouseInfo { get; set; }
    }
}
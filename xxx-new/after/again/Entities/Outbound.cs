﻿using HankWMS_Api.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace again.Entities
{
    /// <summary>
    /// 出库表的结构
    /// </summary>
    public class Outbound:BaseEntity
    {
        /// <summary>
        /// 出库日期
        /// </summary>
        public DateTime Date { get; set; }
        /// <summary>
        /// 出库类型
        /// </summary>
        public string Method { get; set; }
        /// <summary>
        /// 出库人id
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// 出库人信息（导航属性）
        /// </summary>
        public virtual User UserInfo { get; set; }
    }
}
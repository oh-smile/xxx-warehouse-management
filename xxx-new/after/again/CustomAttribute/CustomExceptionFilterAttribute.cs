﻿using HankWMS_Api.CustomException;
using HankWMS_Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Filters;

namespace HankWMS_Api.CustomAttribute
{
    /// <summary>
    /// 自定义的异常筛选器
    /// </summary>
    public class CustomExceptionFilterAttribute : ExceptionFilterAttribute
    {
        /// <summary>
        /// 处理异常
        /// </summary>
        /// <param name="actionExecutedContext"></param>
        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {
            //获取控制器内触发的异常
            var exception = actionExecutedContext.Exception;

            // 判断是否是HttpResponseException这种类型的
            if (exception is ClientException)
            {
                switch ((exception as ClientException).StatusCode)
                {
                    case System.Net.HttpStatusCode.BadRequest:
                        {
                            // 返回的内容
                            var result = new R<string>() { Code = "999999", Message = "请求参数错误" };
                            // 设置控制器内Action最终返回的内容
                            actionExecutedContext.Response = actionExecutedContext.Request.CreateResponse(System.Net.HttpStatusCode.BadRequest, result);
                        }
                        break;
                    default:
                        {
                            // 返回的内容
                            var result = new R<string>() { Code = "999999", Message = "内部服务器错误，请联系管理员" };
                            // 设置控制器内Action最终返回的内容
                            actionExecutedContext.Response = actionExecutedContext.Request.CreateResponse(System.Net.HttpStatusCode.InternalServerError, result);
                        }
                        break;
                }
            }
            else if (exception is BusinessException)
            {

                var businessException = (BusinessException)exception;
                // 返回的内容
                var result = new R<string>() { Code = businessException.Code, Message = businessException.Message };
                // 设置控制器内Action最终返回的内容
                actionExecutedContext.Response = actionExecutedContext.Request.CreateResponse(System.Net.HttpStatusCode.OK, result);
            }
            else
            {
                // 返回的内容
                var result = new R<string>() { Code = "999999", Message = "内部服务器错误，请联系管理员" };
                // 设置控制器内Action最终返回的内容
                actionExecutedContext.Response = actionExecutedContext.Request.CreateResponse(System.Net.HttpStatusCode.InternalServerError, result);
            }
        }

    }
}
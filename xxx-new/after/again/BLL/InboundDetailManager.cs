﻿using again.DAL;
using again.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace again.BLL
{
    /// <summary>
    /// 入库明细业务逻辑层
    /// </summary>
    /// <typeparam name="K"></typeparam>
    public class InboundDetailManager<K>:BaseManager<InboundDetail,K>
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="service"></param>
        public InboundDetailManager(BaseService<InboundDetail>service):base(service)
        {

        }
    }
}
﻿using again.DAL;
using again.Entities;
using again.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace again.BLL
{
    public class SupplierManager<K>:BaseManager<Supplier,K>
    {
        //ervice是一个泛型参数为Supplier的BaseService对象，
        //它是一个供应商服务的基础服务类。
        //base(service)表示调用父类的构造函数，
        //并将service作为参数传递给父类的构造函数。
        //这样做的作用是在SupplierManager类的构造函数中，
        //通过调用父类的构造函数来初始化父类的成员变量和方法，
        //以便在SupplierManager类中可以使用父类的成员变量和方法。
        public SupplierManager(BaseService<Supplier>service):base(service)
        {

        }
        public override Expression<Func<Supplier, bool>> GetWhereExpression(K queryParams)
        {
            if (queryParams == null)
            {
                return p => true;
            }
            var _params = queryParams as SupplierQueryFormModel;
            return p => (p.Name.Contains(_params.Name) || _params.Name == null)
            && (p.ContactPerson.Contains(_params.ContactPerson) || _params.ContactPerson == null);
        }
    }
}
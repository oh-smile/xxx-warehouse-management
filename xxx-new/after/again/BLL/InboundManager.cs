﻿using again.DAL;
using again.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace again.BLL
{
    /// <summary>
    /// 入库业务逻辑层
    /// </summary>
    /// <typeparam name="K"></typeparam>
    public class InboundManager<K>:BaseManager<Inbound,K>
    {
        public InboundManager(BaseService<Inbound>service):base(service)
        {

        }
    }
}
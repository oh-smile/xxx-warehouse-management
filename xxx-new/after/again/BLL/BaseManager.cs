﻿using again.DAL;
using again.Entities;
using again.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace again.BLL
{
    /// <summary>
    /// 业务逻辑层
    /// </summary>
    /// <typeparam name="T">查询的表</typeparam>
    /// <typeparam name="K">查询条件</typeparam>
    /// abstract可以用来修饰类,方法,属性,索引器和时间,这里不包括字段. 
    /// 使用abstrac修饰的类,该类只能作为其他类的基类,不能实例化,
    /// 而且abstract修饰的成员在派生类中必须全部实现,不允许部分实现,否则编译异常
    public abstract class BaseManager<T,K>where T:BaseEntity
    {
        //字段
        private BaseService<T> _service;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="service"></param>
        public BaseManager(BaseService<T> service)
        {
            _service = service;
        }
        public List<T> FindPage(BasePageQueryFormModel<K>model)
        {
            //在此处构建查询条件
            QueryParams<T> queryParams = new QueryParams<T>();
            queryParams.PageIndex=model.PageIndex;
            queryParams.PageSize=model.PageSize;
            queryParams.Expression = GetWhereExpression(model.QueryParams);
            return _service.FindPage(queryParams);
        }
        /// <summary>
        /// 获取单条数据
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public T FindById(int id)
        {
            return _service.FindById(id);
        }
        /// <summary>
        /// 查询所有记录
        /// </summary>
        /// <param name="queryParams"></param>
        /// <returns></returns>
        public List<T> FindAll(K queryParams)
        {
            //这是一个公共的查询条件，恒成立，相当于没有条件
            Expression<Func<T,bool>> whereExpression=GetWhereExpression(queryParams);
            return _service.FindAll(whereExpression);
        }
        /// <summary>
        /// 获取符合条件的记录数
        /// </summary>
        /// <param name="queryParams"></param>
        /// <returns></returns>
        public int Count(K queryParams)
        {
            Expression<Func<T,bool>>whereExpression =GetWhereExpression(queryParams);
            return _service.Count(whereExpression);
        }
        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool Insert(T model)
        {
            return _service.Insert(model);
        }
        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool Update(T model)
        {
            return _service.Update(model);
        }
        public bool Delete(int id)
        {
            return _service.Delete(id);
        }
        /// <summary>
        /// 获取查询条件
        /// </summary>
        /// <param name="queryParams"></param>
        /// <returns></returns>
        public virtual Expression<Func<T,bool>>GetWhereExpression(K queryParams)
        {
            //表示返回所有实体对象
            return t => true;
        }
    }
}
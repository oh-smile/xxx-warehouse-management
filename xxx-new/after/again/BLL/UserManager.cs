﻿using HankWMS_Api.CustomException;
using HankWMS_Api.DAL;
using HankWMS_Api.Entities;
using HankWMS_Api.Models;
using HankWMS_Api.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

namespace HankWMS_Api.BLL
{
    /// <summary>
    /// 业务逻辑层
    /// </summary>
    public class UserManager
    {
        /// <summary>
        /// 数据库访问层
        /// </summary>
        UserService userService = new UserService();

        /// <summary>
        /// 仓库数据库访问层
        /// </summary>
        WarehouseService warehouseService = new WarehouseService();


        /// <summary>
        /// 登录
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public UserDto Login(LoginFormModel model)
        {
            model.Password = Md5Helper.Encrypt(model.Password);
            var user = userService.FindByUserNameAndPassword(model.UserName, model.Password);
            if (user == null)
            {
                return null;
            }
            return new UserDto() { Id = user.Id, UserName = user.UserName, NickName = user.NickName };
        }

        /// <summary>
        /// 添加用户
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool Insert(UserInsertFormModel model)
        {
            // 进行加密
            model.Password = Md5Helper.Encrypt(model.Password);
            // 业务逻辑
            // 判断用户名是否存在
            if (userService.ExistByUserName(model.UserName))
            {
                // 出现同名，返回错误
                throw new BusinessException("010001", "添加失败，用户名已存在");
            }

            return userService.Insert(model);

        }

        /// <summary>
        /// 更新用户
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool Update(UserEditFormModel model)
        {
            //model.Password = Md5Helper.Encrypt(model.Password);
            if (userService.ExistByUserNameAndNotEqualId(model.UserName, model.Id))
            {
                // 出现同名，返回错误
                throw new BusinessException("010002", "更新失败，用户名已存在");
            }
            return userService.Update(model);
        }

        /// <summary>
        /// 删除用户
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public bool Delete(int id)
        {
            // 判断有没有管理仓库，有管理仓库的话，不允许删除
            if (warehouseService.ExistsByUserId(id))
            {
                throw new BusinessException("010003", "删除失败，该用户名下存在仓库，不允许删除");
            }

            return userService.Delete(id);
        }

        public UserDto FindById(int id)
        {
            var user = userService.FindById(id);
            if (user == null)
            {
                return null;
            }
            var userDto = new UserDto() { UserName = user.UserName, Id = user.Id, NickName = user.NickName };
            return userDto;
        }

        /// <summary>
        /// 获取一页数据
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public List<UserDto> FindPage(UserQueryFormModel model)
        {
            List<User> list = userService.FindPage(model);
            return list.Select(user => new UserDto() { UserName = user.UserName, Id = user.Id, NickName = user.NickName }).ToList();
        }

        internal int Count(string searchText)
        {
            return userService.Count(searchText);
        }

        internal bool ChangePwd(ChangePwdFormModel model)
        {
            // 对密码进行加密
            model.Password = Md5Helper.Encrypt(model.Password);

            return userService.Update(model);
        }

        /// <summary>
        /// 获取所有用户的信息
        /// </summary>
        /// <returns></returns>
        internal List<UserDto> GetAll()
        {
            List<User> users = userService.GetAll();
            return users.Select(user => new UserDto() { Id = user.Id, NickName = user.NickName, UserName = user.UserName }).ToList();
        }
    }
}
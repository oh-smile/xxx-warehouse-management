﻿using again.DAL;
using again.Entities;
using again.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace again.BLL
{
    /// <summary>
    /// 商品业务逻辑层
    /// </summary>
    /// <typeparam name="K"></typeparam>
    public class ProductManager<K> : BaseManager<Product, K>
    {
        public ProductManager(BaseService<Product> service) : base(service)
        {

        }

        public override Expression<Func<Product, bool>> GetWhereExpression(K queryParams)
        {
            if(queryParams==null)
            {
                return p => true;
            }
            //Contains()指包含
            var _params = queryParams as ProductQueryFormModel;
            return p => (p.Name.Contains(_params.Name) || _params.Name == null)
            && (p.Code.Contains(_params.Code) || _params.Code == null);
                
        }
    }
}
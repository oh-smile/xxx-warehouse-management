﻿using again.DAL;
using again.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace again.BLL
{
    public class ProductStockManager<K> : BaseManager<ProductStock, K>
    {
        public ProductStockManager(BaseService<ProductStock> service) : base(service)
        {
        }
    }
}
﻿using HankWMS_Api.CustomException;
using HankWMS_Api.DAL;
using HankWMS_Api.Entities;
using HankWMS_Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HankWMS_Api.BLL
{
    /// <summary>
    /// 仓库业务逻辑层
    /// 1. 数据校验
    /// 2. 调用数据访问层，获取数据
    /// 3. 组装数据
    /// </summary>
    public class WarehouseManager
    {

        /// <summary>
        /// 仓库数据访问层
        /// </summary>
        public WarehouseService wareHouseService = new WarehouseService();

        /// <summary>
        /// 用户数据访问层
        /// </summary>
        public UserService userService = new UserService();


        /// <summary>
        /// 获取一页数据
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public List<WarehouseDto> FindPage(WarehouseQueryFormModel model)
        {
            // 调用数据库访问层,返回一页数据（仓库数据的字段 = 数据库中的字段）  
            var list = wareHouseService.FindPage(model);
            return list.Select(warehouse => new WarehouseDto()
            {
                Id = warehouse.Id,
                Name = warehouse.Name,
                Address = warehouse.Address,
                ManagerId = warehouse.UserId,
                ManagerName = warehouse.Manager.NickName,
                PhoneNumber = warehouse.PhoneNumber
            }).ToList();
        }

        /// <summary>
        /// 返回符合条件的记录数
        /// </summary>
        /// <param name="serachText"></param>
        /// <returns></returns>
        public int Count(string serachText)
        {
            // 调用数据库访问层
            return wareHouseService.Count(serachText);
        }

        /// <summary>
        /// 新增仓库
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool Insert(WarehouseInsertFormModel model)
        {
            // 判断仓库管理员是否存在
            if (!userService.ExistById(model.UserId))
            {
                throw new BusinessException("020001", "仓库管理员不存在");
            }

            return wareHouseService.Insert(model);
        }

        /// <summary>
        /// 更新仓库信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public bool Update(WarehouseEditFormModel model)
        {
            // 判断仓库管理员是否存在
            if (!userService.ExistById(model.UserId))
            {
                throw new BusinessException("020001", "仓库管理员不存在");
            }

            return wareHouseService.Update(model);

        }
        internal bool Delete(int id)
        {
            return wareHouseService.Delete(id);
        }


    }
}
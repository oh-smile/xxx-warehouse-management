﻿using again.DAL;
using again.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace again.BLL
{
    /// <summary>
    /// 出库表业务逻辑层
    /// </summary>
    /// <typeparam name="K"></typeparam>
    public class OutboundManager<K> : BaseManager<Outbound, K>
    {
        public OutboundManager(BaseService<Outbound> service) : base(service)
        {
        }
    }
}
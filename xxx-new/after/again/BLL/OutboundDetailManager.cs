﻿using again.DAL;
using again.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace again.BLL
{
    /// <summary>
    /// 出库明细业务逻辑层
    /// </summary>
    /// <typeparam name="K"></typeparam>
    public class OutboundDetailManager<K> : BaseManager<OutboundDetail, K>
    {
        public OutboundDetailManager(BaseService<OutboundDetail> service) : base(service)
        {

        }
    }
}
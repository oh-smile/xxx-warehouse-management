﻿using HankWMS_Api.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HankWMS_Api.Models
{
    /// <summary>
    /// 新增仓库的参数模型
    /// </summary>
    public class WarehouseInsertFormModel
    {
        /// <summary>
        /// 仓库名
        /// </summary>
        [Required(ErrorMessage = "仓库名是必须的")]
        [MaxLength(20, ErrorMessage = "长度不能超过20个字符")]
        public string Name { get; set; }

        /// <summary>
        /// 仓库地址
        /// </summary>
        [MaxLength(50, ErrorMessage = "长度不能超过50个字符")]
        public string Address { get; set; }

        /// <summary>
        /// 仓库管理员id
        /// </summary>
        [Required(ErrorMessage = "仓库管理员是必须的")]
        public int UserId { get; set; }


        /// <summary>
        /// 联系电话
        /// </summary>
        [Required(ErrorMessage = "联系电话是必须的")]
        [MaxLength(20, ErrorMessage = "长度不能超过20个字符")]
        public string PhoneNumber { get; set; }
    }
}
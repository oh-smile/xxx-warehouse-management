﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HankWMS_Api.Models
{
    /// <summary>
    /// 登录参数模型
    /// </summary>
    public class LoginFormModel
    {
        /// <summary>
        /// 用户名
        /// </summary>
        [Required(ErrorMessage = "用户名是必须的")]
        [MinLength(5, ErrorMessage = "用户名长度必须大于等于5")]
        public string UserName { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        [Required(ErrorMessage = "密码是必须的")]
        [MinLength(8, ErrorMessage = "用户名长度必须大于等于8")]
        public string Password { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HankWMS_Api.Models
{
    /// <summary>
    /// 展示层的用户模型
    /// </summary>
    public class UserVO
    {
        /// <summary>
        /// 昵称
        /// </summary>
        public string NickName { get; set; }

        /// <summary>
        /// token
        /// </summary>
        public string Token { get; set; }
    }
}
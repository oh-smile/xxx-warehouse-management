﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HankWMS_Api.Models
{
    /// <summary>
    /// 更新用户的参数模型
    /// </summary>
    public class UserEditFormModel
    {
        /// <summary>
        /// 用户Id
        /// </summary>
        [Required(ErrorMessage = "用户Id是必须的")]
        public int Id { get; set; }

        /// <summary>
        /// 用户名
        /// </summary>
        [Required(ErrorMessage = "用户名是必须的")]
        [MinLength(5, ErrorMessage = "用户名长度不能小于5")]
        public string UserName { get; set; }


        /// <summary>
        /// 昵称
        /// </summary>
        public string NickName { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HankWMS_Api.Models
{
    /// <summary>
    /// 获取用户列表的参数模型
    /// </summary>
    public class UserQueryFormModel
    {
        /// <summary>
        /// 每页记录数
        /// </summary>
        public int PageSize { get; set; } = 10;
        /// <summary>
        /// 获取第几页数据
        /// </summary>
        public int PageIndex { get; set; } = 1;
        /// <summary>
        /// 查询的用户名或昵称
        /// </summary>
        public string SearchText { get; set; } = "";


    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HankWMS_Api.Models
{
    /// <summary>
    /// 修改密码的表单模型
    /// </summary>
    public class ChangePwdFormModel
    {
        /// <summary>
        /// 用户Id
        /// </summary>
        [Required(ErrorMessage = "用户Id是必须的")]
        public int Id { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        [Required(ErrorMessage = "密码是必须的")]
        [MinLength(8, ErrorMessage = "密码长度不能小于8")]
        public string Password { get; set; }
    }
}
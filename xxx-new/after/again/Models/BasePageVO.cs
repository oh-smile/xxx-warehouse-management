﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace again.Models
{
    /// <summary>
    /// 一页数据的模型
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class BasePageVO<T>
    {
        /// <summary>
        /// 满足条件的数据
        /// </summary>
        public List<T> List { get; set; }
        /// <summary>
        /// 总记录数
        /// </summary>
        public int Total { get; set; }
        /// <summary>
        /// 当前第几页
        /// </summary>
        public int PageIndex { get; set; }
        /// <summary>
        /// 每页有多少条记录
        /// </summary>
        public int PageSize { get; set; }
    }
}
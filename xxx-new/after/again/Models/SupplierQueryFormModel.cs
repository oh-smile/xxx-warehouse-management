﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace again.Models
{
    /// <summary>
    /// 查询参数
    /// </summary>
    public class SupplierQueryFormModel
    {
        /// <summary>
        /// 供应商名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 供应商联系人
        /// </summary>
        public string ContactPerson { get; set; }
    }
}
﻿using HankWMS_Api.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HankWMS_Api.Models
{
    /// <summary>
    /// 仓库信息DTO模型
    /// </summary>
    public class WarehouseDto
    {
        /// <summary>
        /// id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// 仓库名
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 仓库地址
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// 仓库管理员id
        /// </summary>
        public int ManagerId { get; set; }

        /// <summary>
        /// 仓库管理员姓名
        /// </summary>
        public string ManagerName { get; set; }

        /// <summary>
        /// 联系电话
        /// </summary>
        public string PhoneNumber { get; set; }
    }
}
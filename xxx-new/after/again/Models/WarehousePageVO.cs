﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HankWMS_Api.Models
{
    /// <summary>
    /// 仓库列表View Object
    /// </summary>
    public class WarehousePageVO
    {
        /// <summary>
        /// 满足条件的数据
        /// </summary>
        public List<WarehouseDto> List { get; set; }
        /// <summary>
        /// 总记录数
        /// </summary>
        public int Total { get; set; }
        /// <summary>
        /// 当前是第几页
        /// </summary>
        public int PageIndex { get; set; }
        /// <summary>
        /// 每页多少条记录
        /// </summary>
        public int PageSize { get; set; }

    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HankWMS_Api.Models
{
    /// <summary>
    /// 更新仓库的参数模型
    /// </summary>
    public class WarehouseEditFormModel : WarehouseInsertFormModel
    {
        [Required(ErrorMessage = "Id是必须的")]
        public int Id { get; set; }
    }
}
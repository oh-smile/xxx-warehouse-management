﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HankWMS_Api.Models
{
    /// <summary>
    /// 表示一个下拉选项
    /// </summary>
    public class OptionVO
    {

        /// <summary>
        /// 下拉选项的值
        /// </summary>
        public int Value { get; set; }

        /// <summary>
        /// 下拉选项的文本
        /// </summary>
        public string Label { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HankWMS_Api.Models
{
    public class UserDto
    {

        /// <summary>
        /// Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// 用户名
        /// </summary>
        public string UserName { get; set; }


        /// <summary>
        /// 昵称
        /// </summary>
        public string NickName { get; set; }
    }
}
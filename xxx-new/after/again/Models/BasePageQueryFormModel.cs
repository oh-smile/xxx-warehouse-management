﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace again.Models
{
    /// <summary>
    /// 获取
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class BasePageQueryFormModel<T>
    {
        /// <summary>
        /// 每页的记录数
        /// </summary>
        public int PageSize { get; set; } = 10;
        /// <summary>
        /// 获取几页数据
        /// </summary>
        public int PageIndex { get; set; } = 1;
        /// <summary>
        /// 查询条件
        /// </summary>
        public T QueryParams { get; set; } = default;
    }
}
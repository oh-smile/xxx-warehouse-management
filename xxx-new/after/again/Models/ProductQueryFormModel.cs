﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace again.Models
{
    /// <summary>
    /// 查询到条件
    /// </summary>
    public class ProductQueryFormModel
    {
        /// <summary>
        /// 查询商品名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 查询商品编码
        /// </summary>
        public string Code { get; set; }
       
    }
}
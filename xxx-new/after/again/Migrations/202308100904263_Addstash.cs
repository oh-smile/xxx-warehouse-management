﻿namespace again.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Addstash : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.InboundDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        InboundId = c.Int(nullable: false),
                        ProductId = c.Int(nullable: false),
                        Quantity = c.Int(nullable: false),
                        WarehouseId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Inbounds", t => t.InboundId, cascadeDelete: false)
                .ForeignKey("dbo.Products", t => t.ProductId, cascadeDelete: false)
                .ForeignKey("dbo.Warehouses", t => t.WarehouseId, cascadeDelete: false)
                .Index(t => t.InboundId)
                .Index(t => t.ProductId)
                .Index(t => t.WarehouseId);
            
            CreateTable(
                "dbo.Inbounds",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Date = c.DateTime(nullable: false),
                        Method = c.String(),
                        UserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: false)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Outbounds",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Date = c.DateTime(nullable: false),
                        Method = c.String(),
                        UserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: false)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Code = c.String(),
                        Brand = c.String(),
                        Specification = c.String(),
                        Color = c.String(),
                        Unit = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.OutboundDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        OutboundId = c.Int(nullable: false),
                        ProductId = c.Int(nullable: false),
                        Quantity = c.Int(nullable: false),
                        WarehouseId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Outbounds", t => t.OutboundId, cascadeDelete: false)
                .ForeignKey("dbo.Products", t => t.ProductId, cascadeDelete: false)
                .ForeignKey("dbo.Warehouses", t => t.WarehouseId, cascadeDelete: false)
                .Index(t => t.OutboundId)
                .Index(t => t.ProductId)
                .Index(t => t.WarehouseId);
            
            CreateTable(
                "dbo.ProductStocks",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProductId = c.Int(nullable: false),
                        warehourseId = c.Int(nullable: false),
                        WarehouseInfo_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Products", t => t.ProductId, cascadeDelete: false)
                .ForeignKey("dbo.Warehouses", t => t.WarehouseInfo_Id)
                .Index(t => t.ProductId)
                .Index(t => t.WarehouseInfo_Id);
            
            CreateTable(
                "dbo.Suppliers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Address = c.String(),
                        ContactPerson = c.String(),
                        ContactModile = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ProductStocks", "WarehouseInfo_Id", "dbo.Warehouses");
            DropForeignKey("dbo.ProductStocks", "ProductId", "dbo.Products");
            DropForeignKey("dbo.OutboundDetails", "WarehouseId", "dbo.Warehouses");
            DropForeignKey("dbo.OutboundDetails", "ProductId", "dbo.Products");
            DropForeignKey("dbo.OutboundDetails", "OutboundId", "dbo.Outbounds");
            DropForeignKey("dbo.InboundDetails", "WarehouseId", "dbo.Warehouses");
            DropForeignKey("dbo.InboundDetails", "ProductId", "dbo.Products");
            DropForeignKey("dbo.InboundDetails", "InboundId", "dbo.Inbounds");
            DropForeignKey("dbo.Outbounds", "UserId", "dbo.Users");
            DropForeignKey("dbo.Inbounds", "UserId", "dbo.Users");
            DropIndex("dbo.ProductStocks", new[] { "WarehouseInfo_Id" });
            DropIndex("dbo.ProductStocks", new[] { "ProductId" });
            DropIndex("dbo.OutboundDetails", new[] { "WarehouseId" });
            DropIndex("dbo.OutboundDetails", new[] { "ProductId" });
            DropIndex("dbo.OutboundDetails", new[] { "OutboundId" });
            DropIndex("dbo.Outbounds", new[] { "UserId" });
            DropIndex("dbo.Inbounds", new[] { "UserId" });
            DropIndex("dbo.InboundDetails", new[] { "WarehouseId" });
            DropIndex("dbo.InboundDetails", new[] { "ProductId" });
            DropIndex("dbo.InboundDetails", new[] { "InboundId" });
            DropTable("dbo.Suppliers");
            DropTable("dbo.ProductStocks");
            DropTable("dbo.OutboundDetails");
            DropTable("dbo.Products");
            DropTable("dbo.Outbounds");
            DropTable("dbo.Inbounds");
            DropTable("dbo.InboundDetails");
        }
    }
}

﻿using again.Entities;
using HankWMS_Api.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace again.DAL
{
    /// <summary>
    /// 通用数据访问层
    /// </summary>
    /// <typeparam name="T">表结构</typeparam>
    public abstract class BaseService<T> where T : BaseEntity
    {
        /// <summary>
        /// EF实例
        /// </summary>
        private HankWMSContext _context = new HankWMSContext();
        /// <summary>
        /// 获取一页记录
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public List<T>FindPage(QueryParams<T> model)
        {
            //set<T>（）获取数据库上下文中与泛型参数T对应的实体集合，返回DbSet<T>对象，用于对数据库实体进行操作
            return _context.Set<T>().Where(model.Expression)
            //.orderby是LINQ查询表达式用于按照实体的id进行升序排序这里的x表示实体对象
                .OrderBy(x => x.Id)
            //.skip是勇于跳过指定数量的实体对象，实现分页功能
                .Skip(model.PageSize*(model.PageIndex-1))
            //.take是获取指定数量的实体对象
                .Take(model.PageSize)
            //.tolist是将查询结果转换成一个List<T>对象，以便进行进一步的处理返回给调用方
                .ToList();
        }
        /// <summary>
        /// 获取单条数据
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public T FindById(int id)
        {
            //.find()获取指定的实体对象
            return _context.Set<T>().Find(id);
        }
        /// <summary>
        /// 查询所有数据
        /// </summary>
        /// <param name="whereExpression"></param>
        /// <returns></returns>
        public List<T> FindAll(Expression<Func<T,bool>>whereExpression=null)
        {
            if(whereExpression == null)
            {
                return _context.Set<T>().ToList();
            }
            return _context.Set<T>().Where(whereExpression).ToList();
        }
        /// <summary>
        /// 获取符合条件的记录数
        /// </summary>
        /// <param name="whereExpression"></param>
        /// <returns></returns>
        public int Count(Expression<Func<T,bool>>whereExpression=null)
        {
            if(whereExpression == null)
            {
                return _context.Set<T>().Count();
            }
            return _context.Set<T>().Where(whereExpression).Count();
        }
        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool Insert(T model)
        {
            try
            {
                _context.Set<T>().Add(model);
                _context.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool Update(T model)
        {
            try
            {
                //Attach(model)将传入的model对象附加到数据库上下文中。这样，数据库上下文就可以跟踪该实体对象的更改。
                _context.Set<T>().Attach(model);
                //将实体对象的状态标记为已修改。这告诉数据库上下文，model对象的属性值已经发生了更改。
                _context.Entry<T>(model).State=System.Data.Entity.EntityState.Modified;
                _context.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool Delete(int id)
        {
            try
            {
                var model=_context.Set<T>().Find(id);
                _context.Set<T>().Remove(model);
                _context.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
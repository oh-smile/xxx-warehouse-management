﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace again.DAL
{
    /// <summary>
    /// 数据访问层的参数模型类，
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class QueryParams<T>
    {
        /// <summary>
        /// 查询的表达式（查询条件）
        /// </summary>
        public Expression<Func<T, bool>> Expression { get; set; }
        /// <summary>
        /// 每页的记录数
        /// </summary>
        public int PageSize { get; set; }= 10;
        /// <summary>
        /// 获取几页数据
        /// </summary>
        public int PageIndex { get; set; } = 1;
    }
}
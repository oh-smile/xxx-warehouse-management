﻿using HankWMS_Api.CustomException;
using HankWMS_Api.Entities;
using HankWMS_Api.Models;
using HankWMS_Api.Utils;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace HankWMS_Api.DAL
{
    /// <summary>
    /// 数据访问层
    /// </summary>
    public class UserService
    {
        private HankWMSContext dbContext = new HankWMSContext();


        /// <summary>
        /// 通过用户名和密码，获取用户信息
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public User FindByUserNameAndPassword(string username, string password)
        {
            return dbContext.Users.Where(user => user.UserName == username && user.Password == password).FirstOrDefault();
        }
        /// <summary>
        /// 根据id返回单个用户信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public User FindById(int id)
        {
            return dbContext.Users.Find(id);
        }

        /// <summary>
        /// 添加新用户
        /// </summary>
        /// <param name="UserInsertFormModel">新用户信息</param>
        public bool Insert(UserInsertFormModel model)
        {
            User user = new User();
            user.UserName = model.UserName;
            user.Password = model.Password;
            user.NickName = model.NickName;
            try
            {
                dbContext.Users.Add(user);
                dbContext.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;

            }
        }

        /// <summary>
        /// 更新用户信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool Update(UserEditFormModel model)
        {
            //User user = new User();
            //user.Id = model.Id;
            //user.UserName = model.UserName;
            //user.NickName = model.NickName;
            //try
            //{
            //    dbContext.Users.Attach(user);
            //    dbContext.Entry(user).State = EntityState.Modified;
            //    dbContext.SaveChanges();
            //    return true;
            //}
            //catch (Exception)
            //{
            //    return false;
            //}

            // 换一种方式，先查询，再修改查询出来的数据,最后再更新上去
            var editUser = dbContext.Users.Find(model.Id);
            editUser.UserName = model.UserName;
            editUser.NickName = model.NickName;
            try
            {
                dbContext.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }


        }

        /// <summary>
        /// 删除用户
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Delete(int id)
        {
            try
            {
                var user = dbContext.Users.Find(id);
                dbContext.Users.Remove(user);
                dbContext.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// 获取一页数据
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public List<User> FindPage(UserQueryFormModel model)
        {
            try
            {
                return dbContext.Users.Where(user => user.NickName.Contains(model.SearchText)
                                    || user.UserName.Contains(model.SearchText)
                                    || model.SearchText == null
                                    || model.SearchText == "")
                                .OrderBy(user => user.Id)
                                .Skip(model.PageSize * (model.PageIndex - 1))
                                .Take(model.PageSize)
                                .ToList();
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// 获取符合条件的记录数
        /// </summary>
        /// <param name="searchText"></param>
        /// <returns></returns>
        public int Count(string searchText)
        {
            return dbContext.Users.Where(user => user.NickName.Contains(searchText)
                                    || user.UserName.Contains(searchText)
                                    || searchText == null
                                    || searchText == "").Count();
        }
        /// <summary>
        /// 判断用户名是否存在
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        public bool ExistByUserName(string userName)
        {
            return dbContext.Users.Where(user => user.UserName == userName).Count() > 0;
        }

        /// <summary>
        /// 判断用户名是否存在，排除掉指定id对应的记录
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool ExistByUserNameAndNotEqualId(string userName, int id)
        {
            return dbContext.Users.Where(user => user.UserName == userName && user.Id != id).Count() > 0;
        }
        /// <summary>
        /// 更新用户信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool Update(ChangePwdFormModel model)
        {
            // 换一种方式，先查询，再修改查询出来的数据,最后再更新上去
            var editUser = dbContext.Users.Find(model.Id);
            if (editUser == null)
            {
                throw new BusinessException("010003", "待更新的用户不存在");
            }
            editUser.Password = model.Password;
            try
            {
                dbContext.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }


        /// <summary>
        ///  判断用户是否存在，根据用户id
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public bool ExistById(int userId)
        {
            return dbContext.Users.Where(user => user.Id == userId).Count() > 0;
        }

        /// <summary>
        /// 获取所有用户信息
        /// </summary>
        /// <returns></returns>
        public List<User> GetAll()
        {
            return dbContext.Users.ToList();
        }
    }
}
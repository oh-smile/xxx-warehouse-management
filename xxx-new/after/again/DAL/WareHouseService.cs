﻿using HankWMS_Api.CustomException;
using HankWMS_Api.Entities;
using HankWMS_Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HankWMS_Api.DAL
{
    /// <summary>
    /// 仓库数据库访问层
    /// </summary>
    public class WarehouseService
    {
        // ef实例
        private HankWMSContext _context = new HankWMSContext();

        /// <summary>
        /// 获取一页数据
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public List<Warehouse> FindPage(WarehouseQueryFormModel model)
        {
            // 查询数据库
            return _context.Warehouses.Where(warehouse => warehouse.Name.Contains(model.SearchText)
                                        || warehouse.Manager.NickName.Contains(model.SearchText)
                                        || model.SearchText == null
                                        || model.SearchText == "")
                               .OrderBy(warehouse => warehouse.Id)
                               .Skip(model.PageSize * (model.PageIndex - 1))
                               .Take(model.PageSize)
                               .ToList();
        }

        /// <summary>
        /// 返回符合条件的记录数
        /// </summary>
        /// <param name="searchText"></param>
        /// <returns></returns>
        public int Count(string searchText)
        {
            return _context.Warehouses.Where(warehouse => warehouse.Name.Contains(searchText)
                                        || warehouse.Manager.NickName.Contains(searchText)
                                        || searchText == null
                                        || searchText == "")
                                       .Count();
        }

        /// <summary>
        ///  添加仓库
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        internal bool Insert(WarehouseInsertFormModel model)
        {
            Warehouse warehouse = new Warehouse();
            warehouse.Name = model.Name;
            warehouse.Address = model.Address;
            warehouse.UserId = model.UserId;
            warehouse.PhoneNumber = model.PhoneNumber;

            _context.Warehouses.Add(warehouse);
            try
            {
                _context.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }

        /// <summary>
        /// 更新仓库信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public bool Update(WarehouseEditFormModel model)
        {
            // 先查询
            var warehouse = _context.Warehouses.Find(model.Id);
            if (warehouse == null)
            {
                throw new BusinessException("020002", "待更新的仓库不存在");
            }
            // 再修改
            warehouse.Name = model.Name;
            warehouse.Address = model.Address;
            warehouse.UserId = model.UserId;
            warehouse.PhoneNumber = model.PhoneNumber;
            
            try
            {
                // 再同步到数据库
                _context.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// 删除仓库信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        internal bool Delete(int id)
        {
            var warehouse = _context.Warehouses.Find(id);
            _context.Warehouses.Remove(warehouse);
            try
            {
                _context.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// 判断是否存在指定用户的仓库
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        internal bool ExistsByUserId(int id)
        {
            return _context.Warehouses.Where(warehouse => warehouse.UserId == id).Count() > 0;
        }
    }
}
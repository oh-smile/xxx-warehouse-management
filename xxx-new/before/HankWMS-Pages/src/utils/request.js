// 封装axios
import axios from 'axios'
import { ElNotification } from 'element-plus'
import router from '../router'


// 创建一个实例
const instance = axios.create({
    baseURL: 'http://localhost:57552/api/',
    timeout: 1000
});

// 配置实例
// 添加请求拦截器
instance.interceptors.request.use(function (config) {
    // 在发送请求之前做些什么
    // 设置一下头部，携带token
    config.headers["Authorization"] = sessionStorage.getItem('Authorization')

    return config;
}, function (error) {
    // 对请求错误做些什么
    return Promise.reject(error);
});

// 添加响应拦截器
instance.interceptors.response.use(function (response) {
    // 2xx 范围内的状态码都会触发该函数。
    // 对响应数据做点什么,进行简化，直接把服务器返回的结果，返回出去
    return response.data;
}, function (error) {
    let msg = ''
    if (!error.response) {
        msg = '网络异常，未接收到服务器的响应';
    } else {
        switch (error.response.status) {
            case 401:
                msg = '登录超时，请重新登录'
                router.push('/login')
                break;
            case 403:
                msg = '禁止访问'
                break;
            case 400:
                msg = '请求参数错误'
                break;
            case 500:
                msg = '服务器错误，请联系管理员'
                break;
            default:
                msg = '未处理错误'
                break;
        }
    }
    ElNotification({
        title: '错误',
        message: msg,
        type: 'error'
    })
    // 超出 2xx 范围的状态码都会触发该函数。
    // 对响应错误做点什么
    return Promise.reject(error);
});



// 导出实例
export default instance
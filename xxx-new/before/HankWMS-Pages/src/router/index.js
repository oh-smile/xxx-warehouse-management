import { createRouter, createWebHistory } from 'vue-router'
import LoginView from '../views/LoginView.vue'
import HomeView from '../views/HomeView.vue'
import WorkbenchView from '../views/WorkbenchView.vue'
import UserView from '../views/UserView.vue'
import WarehouseView from '../views/WarehouseView.vue'
import ProductView from '../views/ProductView.vue'
const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      alias: '/login',
      name: 'login',
      component: LoginView
    },
    {
      path: '/home',
      name: 'home',
      component: HomeView,
      children: [
        {
          path: '/workbench',
          name: 'workbench',
          component: WorkbenchView
        },
        {
          path: '/user',
          name: 'user',
          component: UserView
        },
        {
          path: '/warehouse',
          name: 'warehouse',
          component: WarehouseView
        }
        ,{
          path:'/product',
          name:'product',
          component:ProductView
        }
      ]
    }, {
      path: '/:pathMatch(.*)*',
      name: 'notfoundpage',
      component: () => import('../views/NotFound.vue')
    }
  ]
})

// 路由守卫
router.beforeEach((to, from) => {
  // ...
  // 判断有没有登录的规则
  let isAuth = sessionStorage.getItem('Authorization') ? true : false

  //跳转到非登录界面且非404页面且未登录
  if (to.name != 'login' && to.name != 'notfoundpage' && !isAuth)
    // 返回 false 以取消导航
    return { name: 'login' }
})

export default router

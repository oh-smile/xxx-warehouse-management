import './assets/main.css'

import { createApp } from 'vue'
import { createPinia } from 'pinia'

import App from './App.vue'
import router from './router'

const app = createApp(App)
// 引入element-plus组件
import EP from 'element-plus'
// 引入element-plus样式
import 'element-plus/dist/index.css'
// 使用中文
import zhCn from 'element-plus/dist/locale/zh-cn.mjs'
app.use(EP, {
  locale: zhCn,
})
// 引入Icon
import * as ElementPlusIconsVue from '@element-plus/icons-vue'
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
    app.component(key, component)
}
app.use(createPinia())
app.use(router)

app.mount('#app')
